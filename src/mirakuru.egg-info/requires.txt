psutil>=4.0.0

[docs]
sphinx

[tests]
pytest==3.8.2
pytest-cov==2.6.0
mock==2.0.0
python-daemon==2.2.0
